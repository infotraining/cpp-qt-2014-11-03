#ifndef BACKGROUNDTASK_H
#define BACKGROUNDTASK_H

#include <QThread>

class BackgroundTask : public QThread
{
    Q_OBJECT
public:
    explicit BackgroundTask(QObject *parent = 0);
    void run();

signals:
    void started();
    void notifyProgress(int);
    void done();

public slots:

};

#endif // BACKGROUNDTASK_H
