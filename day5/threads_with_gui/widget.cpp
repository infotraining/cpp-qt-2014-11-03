#include "widget.h"
#include "ui_widget.h"
#include "backgroundtask.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    BackgroundTask* task = new BackgroundTask(this);

    connect(ui->btnStart, SIGNAL(clicked()), task, SLOT(start()));

    connect(ui->btnExit, SIGNAL(clicked()), this, SLOT(close()));

    connect(task, SIGNAL(notifyProgress(int)), ui->progressBar, SLOT(setValue(int)));

    connect(task, &BackgroundTask::started, [this] () { ui->btnExit->setDisabled(true);});

    connect(task, &BackgroundTask::done, [this] () { ui->btnExit->setDisabled(false);});
}

Widget::~Widget()
{
    delete ui;
}
