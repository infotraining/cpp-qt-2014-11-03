#-------------------------------------------------
#
# Project created by QtCreator 2014-11-07T11:18:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = threads_with_gui
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    backgroundtask.cpp

HEADERS  += widget.h \
    backgroundtask.h

FORMS    += widget.ui

CONFIG += C++11
