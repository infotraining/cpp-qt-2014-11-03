#include "backgroundtask.h"

BackgroundTask::BackgroundTask(QObject *parent) :
    QThread(parent)
{
}

void BackgroundTask::run()
{
    emit started();
    for (int i = 0 ; i <= 100 ; i += 5)
    {
        msleep(200);
        emit notifyProgress(i);
    }
    emit done();
}
