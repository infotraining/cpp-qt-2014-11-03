#include "widget.h"
#include "ui_widget.h"
#include <QNetworkReply>
#include <QNetworkRequest>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    //resize(200, 300);
    showMaximized();
    ui->setupUi(this);
    model = new QStandardItemModel(0, 4);
    manager = new QNetworkAccessManager;
    QStringList labels;
    labels << "Nazwa" << "przelicznik" << "kod" << "wartość";
    model->setHorizontalHeaderLabels(labels);
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);

    connect(ui->lineEdit, SIGNAL(returnPressed()), ui->btnOpen, SIGNAL(clicked()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnOpen_clicked()
{
    reply = manager->get(QNetworkRequest(ui->lineEdit->text()));
    connect(reply, SIGNAL(finished()), this, SLOT(updateModel()));
}

void Widget::updateModel()
{
    model->clear();
    QStringList labels;
    labels << "Nazwa" << "przelicznik" << "kod" << "wartość";
    model->setHorizontalHeaderLabels(labels);
    QXmlInputSource source(reply);
    QXmlSimpleReader reader;
    QDomDocument doc;
    if (!doc.setContent(&source, &reader))
        qDebug() << "Error parsing xml";

    QDomElement el = doc.documentElement();
    QDomNodeList nodes = el.elementsByTagName("pozycja");
    for(int i = 0 ; i < nodes.length() ; ++i)
    {
        QList<QStandardItem*> row;
        row << new QStandardItem(
                    nodes.at(i).toElement().firstChildElement("nazwa_waluty").text());
        row << new QStandardItem(
                    nodes.at(i).toElement().firstChildElement("przelicznik").text());
        row << new QStandardItem(
                    nodes.at(i).toElement().firstChildElement("kod_waluty").text());
        row << new QStandardItem(
                    nodes.at(i).toElement().firstChildElement("kurs_sredni").text());
        model->appendRow(row);
    }
}
