#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QStandardItemModel>
#include <QNetworkAccessManager>
#include <QtNetwork>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QDomText>
#include <QFile>
#include <QTextStream>
#include <QXmlInputSource>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    QStandardItemModel *model;
    QNetworkAccessManager *manager;
    QNetworkReply* reply;

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_btnOpen_clicked();
    void updateModel();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
