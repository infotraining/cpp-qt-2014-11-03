#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDialog>
#include <QFileDialog>
#include <QList>
#include <QStringList>
#include <QImage>
#include <QtMath>
#include <QGridLayout>
#include <QStandardPaths>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    layout_ = new QGridLayout;
    ui->centralWidget->setLayout(layout_);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_Files_triggered()
{
    QStringList files_list = QFileDialog::getOpenFileNames(this,
                                                           "Select images",
                                                           QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)
                                                           , "*.*");
    if (files_list.length() == 0) return;
    foreach(QString filename, files_list)
    {
        orig_images << QImage(filename);
    }

    labels.clear();
    int dim_ = qCeil(qSqrt(files_list.length()));
    for(int i = 0 ; i < dim_ ; ++i)
    {
        for (int j = 0 ; j < dim_ ; ++j)
        {
            QLabel* imgLabel = new QLabel();
            imgLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
            layout_->addWidget(imgLabel, i, j);
            labels << imgLabel;

        }
    }

    for(int i = 0 ; i < orig_images.length(); ++i)
    {
        QImage resized = orig_images[i].scaled(labels[i]->size());
        labels[i]->setPixmap(QPixmap::fromImage(resized));
    }
}

void MainWindow::on_actionExit_triggered()
{

}
