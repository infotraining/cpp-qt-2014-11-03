#include <QCoreApplication>
#include <QDebug>
#include "mythread.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug() << "Main thread " << QThread::currentThreadId();
    MyThread th1, th2, th3;
    th1.start();
    th2.start();
    th3.start();
    qDebug() << "Still in mainthread";
    th1.wait();
    th2.wait();
    th3.wait();
    return 0;
    return a.exec();
}
