#-------------------------------------------------
#
# Project created by QtCreator 2014-11-07T10:33:43
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = threads_simple
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mythread.cpp

HEADERS += \
    mythread.h
