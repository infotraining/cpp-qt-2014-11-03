#include "mythread.h"
#include <QDebug>

MyThread::MyThread(QObject *parent) :
    QThread(parent)
{
}

void MyThread::run()
{
    for(int i = 0 ; i < 10; ++i)
    {
        msleep(500);
        qDebug() << "Welcome in thread " << this->currentThreadId();
    }
}
