#include <QCoreApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QDomText>
#include <QFile>
#include <QTextStream>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QDomDocument doc;

    QDomElement root = doc.createElement("root");
    doc.appendChild(root);

    QDomElement el = doc.createElement("foo");
    el.setAttribute("name", "shmoo" );
    root.appendChild(el);

    qDebug() << doc.toString();
    QFile f("simple.xml");
    f.open(QIODevice::WriteOnly);
    QTextStream stream(&f);
    stream << doc.toString();
    f.close();
    // parsing

    f.open(QIODevice::ReadOnly);
    QDomDocument new_doc;
    if (!new_doc.setContent(&f))
        qDebug() << "Error parsing xml";

    el = new_doc.documentElement();
    QDomNode node = el.firstChild();
    while(!node.isNull())
    {
        if (node.isElement())
        {
            qDebug() << node.toElement().tagName() << " " << node.toElement().text();
        }
        if (node.isText())
        {
            qDebug() << node.toText().data();
        }
        node = node.nextSibling();
    }


    return 0;
}
