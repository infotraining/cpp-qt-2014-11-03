/********************************************************************************
** Form generated from reading UI file 'mainw.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINW_H
#define UI_MAINW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainW
{
public:
    QAction *actionOpen_files;
    QAction *actionExit;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainW)
    {
        if (MainW->objectName().isEmpty())
            MainW->setObjectName(QStringLiteral("MainW"));
        MainW->resize(400, 300);
        actionOpen_files = new QAction(MainW);
        actionOpen_files->setObjectName(QStringLiteral("actionOpen_files"));
        actionExit = new QAction(MainW);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainW);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        MainW->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainW);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainW->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainW);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainW->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainW);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainW->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen_files);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        mainToolBar->addAction(actionOpen_files);
        mainToolBar->addAction(actionExit);

        retranslateUi(MainW);

        QMetaObject::connectSlotsByName(MainW);
    } // setupUi

    void retranslateUi(QMainWindow *MainW)
    {
        MainW->setWindowTitle(QApplication::translate("MainW", "MainW", 0));
        actionOpen_files->setText(QApplication::translate("MainW", "Open files", 0));
        actionOpen_files->setShortcut(QApplication::translate("MainW", "Ctrl+O", 0));
        actionExit->setText(QApplication::translate("MainW", "Exit", 0));
        actionExit->setShortcut(QApplication::translate("MainW", "Ctrl+W", 0));
        menuFile->setTitle(QApplication::translate("MainW", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainW: public Ui_MainW {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINW_H
