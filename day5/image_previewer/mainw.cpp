#include "mainw.h"
#include "ui_mainw.h"
#include <QFileDialog>
#include <functional>

void MainW::calc_size()
{
    QSize main_window = ui->centralWidget->size();
    label_size_ = QSize((main_window.width()/dim_)-20, (main_window.height()/dim_)-20);
}


QImage MainW::load_image(QString filename)
{
    //qDebug() << "load_image " << filename;
    QImage img(filename);
    return img;
}

QImage MainW::resize_image(QImage img)
{
    //qDebug() << "resize_image " << img.size();
    return img.scaled(label_size_, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
}

void MainW::resizeEvent(QResizeEvent *e)
{
    //qDebug() << "resize_event " << labels_list.size();
    calc_size();
    if (resize_watch->isFinished())
    {
        resize_watch->cancel();
        //qDebug() << "resize cancelled";
    }
    resize_watch->setFuture(QtConcurrent::mapped(orig_images_list,
            std::bind(&MainW::resize_image, this, std::placeholders::_1)));
}

MainW::MainW(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainW)
{
    ui->setupUi(this);
    layout =  new QGridLayout();
    layout->setSizeConstraint(QLayout::SetNoConstraint);
    ui->centralWidget->setLayout(layout);
    load_watch = new QFutureWatcher<QImage>();
    resize_watch = new QFutureWatcher<QImage>();

    connect(load_watch, SIGNAL(resultReadyAt(int)),
            this, SLOT(setImage(int)));

    connect(resize_watch, SIGNAL(resultReadyAt(int)),
            this, SLOT(setLabel(int)));
}

MainW::~MainW()
{
    delete ui;
}

void MainW::on_actionExit_triggered()
{
    close();
}

void MainW::on_actionOpen_files_triggered()
{
    QStringList list_of_files = QFileDialog::getOpenFileNames(this,
                                                  "Select images",
                                                  QStandardPaths::writableLocation(QStandardPaths::PicturesLocation),
                                                  "*.*");
    if (list_of_files.count() == 0)
        return;

    initialize_labels(list_of_files.count());
    calc_size();
    orig_images_list.resize(list_of_files.size());
    resized_images_list.resize(list_of_files.size());

    if (load_watch->isRunning())
    {
        //qDebug() << "loading in progress??";
        load_watch->cancel();
        load_watch->waitForFinished();
    }


    load_watch->setFuture(QtConcurrent::mapped(list_of_files,
                                   std::bind(&MainW::load_image, this, std::placeholders::_1)));
    //qDebug() << "after set load_watch ";

}

void MainW::setLabel(int i)
{
    resized_images_list[i] = resize_watch->resultAt(i);
    //qDebug() << "resized to:" << resized_images_list[i].size();
    labels_list[i]->setPixmap(QPixmap::fromImage(resized_images_list[i]));
}

void MainW::setImage(int i)
{
    qDebug() << "setImage: " << i << " lab_size: " << label_size_;
    QImage img = load_watch->resultAt(i);
    orig_images_list[i] = img;
    resized_images_list[i] = img.scaled(label_size_, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    labels_list[i]->setPixmap(QPixmap::fromImage(resized_images_list[i]));
}

void MainW::initialize_labels(int i)
{
    dim_ = qCeil(qSqrt(i));
    qDeleteAll(labels_list);
    labels_list.clear();
    orig_images_list.clear();    
    for (int j = 0 ; j < dim_ ; ++j)
    {
        for (int k = 0 ; k < dim_ ; ++k)
        {
            QLabel* imgLabel = new QLabel;
            imgLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
            //imgLabel->setFixedSize(100,100);
            layout->addWidget(imgLabel, j, k);
            labels_list.append(imgLabel);            
        }
    }
    label_size_ = labels_list[0]->size();
}
