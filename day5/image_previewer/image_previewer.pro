#-------------------------------------------------
#
# Project created by QtCreator 2013-08-09T19:32:04
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = image_previewer
TEMPLATE = app


SOURCES += main.cpp\
        mainw.cpp

HEADERS  += mainw.h

FORMS    += mainw.ui
QMAKE_CXXFLAGS = -std=c++11
CONFIG += C++11
