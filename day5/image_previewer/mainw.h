#ifndef MAINW_H
#define MAINW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QLabel>
#include <QImage>
#include <QStringList>
#include <QVector>
#include <QList>
#include <QImage>
#include <QPixmap>
#include <QtConcurrent>
#include <QFutureWatcher>

namespace Ui {
class MainW;
}

class MainW : public QMainWindow
{    
    Q_OBJECT
    int dim_;
    QSize label_size_;
    void calc_size();
    QFutureWatcher<QImage> *load_watch;
    QFutureWatcher<QImage> *resize_watch;
    QGridLayout* layout;
    QVector<QImage> orig_images_list;
    QVector<QImage> resized_images_list;
    QList<QLabel*> labels_list;
    QImage load_image(QString filename);
    QImage resize_image(QImage img);
    void resizeEvent(QResizeEvent *e);
    
public:
    explicit MainW(QWidget *parent = 0);
    ~MainW();
    
private slots:
    void on_actionExit_triggered();
    void on_actionOpen_files_triggered();

    void setLabel(int i);
    void setImage(int i);
    void initialize_labels(int i);

private:
    Ui::MainW *ui;
};

#endif // MAINW_H
