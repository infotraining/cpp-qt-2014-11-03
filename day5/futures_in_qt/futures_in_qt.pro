#-------------------------------------------------
#
# Project created by QtCreator 2014-11-07T11:46:32
#
#-------------------------------------------------

QT       += core concurrent

QT       -= gui

TARGET = futures_in_qt
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

CONFIG += c++11
