#include <QCoreApplication>
#include <QtConcurrent>
#include <QFutureWatcher>
#include <QThread>
#include <QDebug>
#include <QtMath>
#include <QTime>
#include <QVector>

int answer()
{
    QThread::sleep(5);
    return 42;
}

bool is_prime(unsigned long i)
{
    if (i < 2) return false;
    if (i == 2) return true;
    if (i % 2 == 0) return false;
    for (unsigned long div = 3 ; div <= (unsigned long)qSqrt(i) ; div += 2)
    {
        if (i % div == 0) return false;
    }
    return true;
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
//    qDebug() << "Asking:";
//    QFuture<int> ans = QtConcurrent::run(answer);
//    QFutureWatcher<int> watcher;
//    QObject::connect(&watcher, SIGNAL(finished()),
//                     &a, SLOT(quit()));

//    watcher.setFuture(ans);
//    qDebug() << "Another task...";

//    a.exec();
//    qDebug() << "Answer = " << ans.result();
    QVector<unsigned long> numbers;
    QVector<unsigned long> res;
    for (unsigned long i = 90000000000 ; i < 90000010000 ; ++i)
        numbers << i;

    QTime timer;
    timer.start();

    foreach(unsigned long i, numbers)
    {
        if (is_prime(i))
            res << i;
    }
    qDebug() << "N of primes: " << res.length() << " " << timer.elapsed() << " ms";

    timer.start();
    QFuture<unsigned long> res3 = QtConcurrent::filtered(numbers, is_prime);
    //res3.waitForFinished();
    QFutureWatcher<unsigned long> watcher;

    QObject::connect(&watcher, &QFutureWatcher<unsigned long>::resultReadyAt, [&res3](int i) { qDebug() << res3.resultAt(i); });

    watcher.setFuture(res3);
    //qDebug() << "N of primes (filtered): " << res3.results().length() << " " << timer.elapsed() << " ms";

//    timer.start();
//    QFuture<void> res2 = QtConcurrent::filter(numbers, is_prime);
//    res2.waitForFinished();
//    qDebug() << "N of primes (filter): " << numbers.length() << " " << timer.elapsed() << " ms";


    a.exec();
    return 0;
}
