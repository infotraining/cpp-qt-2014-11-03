#include <QCoreApplication>
#include <QtNetwork>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QDomText>
#include <QFile>
#include <QTextStream>
#include <QXmlInputSource>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QNetworkAccessManager manager;

    QNetworkReply *reply = manager.get(QNetworkRequest(QString("http://nbp.pl/kursy/xml/a217z141107.xml")));

    QObject::connect(reply, SIGNAL(finished()), &a, SLOT(quit()));
    a.exec();
    //qDebug() << reply->readAll();
    QXmlInputSource source(reply);
    QXmlSimpleReader reader;
    QDomDocument doc;
    if (!doc.setContent(&source, &reader))
        qDebug() << "Error parsing xml";

    QDomElement el = doc.documentElement();
    QDomNodeList nodes = el.elementsByTagName("pozycja");
    for(int i = 0 ; i < nodes.length() ; ++i)
    {
        qDebug() << nodes.at(i).toElement().firstChildElement("kod_waluty").text() <<
                    " " <<
                    nodes.at(i).toElement().firstChildElement("kurs_sredni").text();
    }

//    QDomNode node = el.firstChild();
//    while(!node.isNull())
//    {
//        if (node.isElement())
//        {
//            qDebug() << node.toElement().tagName() << " " << node.toElement().text();
//        }
//        if (node.isText())
//        {
//            qDebug() << node.toText().data();
//        }
//        node = node.nextSibling();
//    }

    return 0;
}
