#-------------------------------------------------
#
# Project created by QtCreator 2014-11-06T10:48:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = custom_widget
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    spinslider.cpp

HEADERS  += widget.h \
    spinslider.h

FORMS    +=

CONFIG += c++11
