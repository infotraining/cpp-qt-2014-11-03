#ifndef SPINSLIDER_H
#define SPINSLIDER_H

#include <QWidget>
#include <QSpinBox>
#include <QSlider>

class SpinSlider : public QWidget
{
    Q_OBJECT

    QSlider *slider;
    QSpinBox *sbox;

public:
    explicit SpinSlider(QWidget *parent = 0);
    int value() const;
    void setRange(int start, int end);

signals:
    void valueChanged(int);

public slots:
    void setValue(int);

};

#endif // SPINSLIDER_H
