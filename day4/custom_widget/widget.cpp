#include "widget.h"
#include <QSpinBox>
#include <QSlider>
#include <QtWidgets>
#include "spinslider.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    auto l = new QVBoxLayout();
    auto slider = new SpinSlider();
    auto label  = new QLabel();
    l->addWidget(slider);
    l->addWidget(label);
    this->setLayout(l);

    connect(slider, SIGNAL(valueChanged(int)),
            label, SLOT(setNum(int)));

}

Widget::~Widget()
{

}
