#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSqlRelationalTableModel>
#include <QSqlRelationalDelegate>
#include <QSqlError>
#include <QSqlRecord>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

    QSqlRelationalTableModel* model;
    QSqlDatabase db;

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H

