#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../../Northwind.sl3");
    db.open();

    model = new QSqlRelationalTableModel(this, db);
    model->setTable("Products");
    model->setEditStrategy(QSqlRelationalTableModel::OnFieldChange);
    model->setRelation(model->fieldIndex("SupplierID"),
                       QSqlRelation("Suppliers", "SupplierID", "CompanyName"));
    model->setRelation(model->fieldIndex("CategoryID"),
                       QSqlRelation("Categories", "CategoryID", "CategoryName"));
    ui->tableView->setItemDelegate(new QSqlRelationalDelegate(ui->tableView));
    model->select();
    ui->tableView->setModel(model);
    ui->tableView->hideColumn(0);
    qDebug() << db.lastError();
}

Widget::~Widget()
{
    delete ui;
}
