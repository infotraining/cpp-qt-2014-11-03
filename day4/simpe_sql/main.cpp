#include <QCoreApplication>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../../Northwind.sl3");
    if (!db.open()) return -1;

    qDebug() << "Database opened";
    qDebug() << db.tables();

    QSqlQuery query;

    query.exec("SELECT * FROM Products");
    while(query.next())
    {
        qDebug() << query.value(0).toString() << query.value(1).toString();
    }

    return 0;
    return a.exec();
}
