#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSql>
#include <QSqlQueryModel>
#include <QSqlDatabase>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

    QSqlQueryModel* model;
    QSqlDatabase db;

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_lineEdit_returnPressed();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
