#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QSqlError>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../../Northwind.sl3");
    db.open();

    model = new QSqlQueryModel(this);
    model->setQuery(ui->lineEdit->text(), db);
    ui->tableView->setModel(model);
    qDebug() << db.lastError();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_lineEdit_returnPressed()
{
    model->setQuery(ui->lineEdit->text(), db);
    qDebug() << db.lastError();
}
