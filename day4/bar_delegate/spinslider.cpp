#include "spinslider.h"
#include <QHBoxLayout>
#include <QStyle>

SpinSlider::SpinSlider(QWidget *parent) :
    QWidget(parent)
{
    QHBoxLayout* l = new QHBoxLayout();
    slider = new QSlider();
    sbox = new QSpinBox();
    slider->setOrientation(Qt::Horizontal);
    this->setAutoFillBackground(true);
    l->addWidget(sbox);
    l->addWidget(slider);
    l->setMargin(0);
    this->setLayout(l);

    connect(sbox, SIGNAL(valueChanged(int)),
            slider, SLOT(setValue(int)));

    connect(slider, SIGNAL(valueChanged(int)),
            sbox, SLOT(setValue(int)));

    connect(slider, SIGNAL(valueChanged(int)),
            this, SIGNAL(valueChanged(int)));
}

int SpinSlider::value() const
{
    return slider->value();
}

void SpinSlider::setRange(int start, int end)
{
    slider->setRange(start, end);
    sbox->setRange(start, end);
}

void SpinSlider::setValue(int v)
{
    slider->setValue(v);
}

