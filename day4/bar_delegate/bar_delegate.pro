#-------------------------------------------------
#
# Project created by QtCreator 2014-11-06T09:26:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bar_delegate
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    bardelegate.cpp \
    spinslider.cpp

HEADERS  += widget.h \
    bardelegate.h \
    spinslider.h

FORMS    += widget.ui

CONFIG += c++11
