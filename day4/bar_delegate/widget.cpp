#include "widget.h"
#include "ui_widget.h"
#include <QStandardItemModel>
#include "bardelegate.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    auto model = new QStandardItemModel(0, 2);
    QStringList headers;
    headers << "currency" << "value";
    model->setHorizontalHeaderLabels(headers);

    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);

    model->setItem(0,0, new QStandardItem("PLN"));
    model->setItem(0,1, new QStandardItem("25"));
    model->setItem(1,0, new QStandardItem("USD"));
    model->setItem(1,1, new QStandardItem("45"));
    model->setItem(2,0, new QStandardItem("GPB"));
    model->setItem(2,1, new QStandardItem("65"));
    model->setItem(3,0, new QStandardItem("EUR"));
    model->setItem(3,1, new QStandardItem("15"));

    auto delegate = new BarDelegate();
    ui->tableView->setItemDelegateForColumn(1, delegate);
    ui->tableView->verticalHeader()->setDefaultSectionSize(40);

}

Widget::~Widget()
{
    delete ui;
}
