#include "bardelegate.h"
#include <QPainter>
#include <QSlider>
#include "spinslider.h"

BarDelegate::BarDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

void BarDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();

    int value = index.data().toInt();
    float f = value/100.0;

    if (option.state & QStyle::State_Selected)
    {
        //selected
        painter->setBrush(QColor(0,255,0));
        painter->fillRect(option.rect, option.palette.base());
    }
    else
    {
        painter->setBrush(QColor(200,200,255));
    }


    painter->drawRect(QRect(option.rect.x() +2 ,
                            option.rect.y() +2,
                            option.rect.width()*f -4,
                            option.rect.height()-4));

    painter->drawText(option.rect, Qt::AlignCenter, QString::number(value));

    painter->restore();
}

QSize BarDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(50,50);
}

QWidget *BarDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(index.data().canConvert<int>())
    {
        //auto slider = new QSlider(parent);
        //slider->setOrientation(Qt::Horizontal);
        //slider->setAutoFillBackground(true);
        //return slider;
        auto ss = new SpinSlider(parent);
        return ss;
    }
}

void BarDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if(index.data().canConvert<int>())
    {
        int value = index.data().toInt();
        static_cast<SpinSlider*>(editor)->setValue(value);
    }
}

void BarDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData(index, static_cast<SpinSlider*>(editor)->value());
}

void BarDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}
