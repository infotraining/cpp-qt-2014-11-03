#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("My Fancy Editor [*]");
    connect(ui->plainTextEdit->document(), SIGNAL(modificationChanged(bool)),
            this, SLOT(setWindowModified(bool)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionSave_triggered()
{
    QString filename = QFileDialog::getSaveFileName(this, "File to save",
                                                    ".",
                                                    "All files (*.*)");
    if (filename == "") return;
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        ui->statusBar->showMessage(QString("Error saving file %1").arg(filename));
    }
    ui->statusBar->showMessage(QString("Saving file %1").arg(filename));
    QTextStream stream(&file);
    stream << ui->plainTextEdit->toPlainText();
    setWindowModified(false);
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionOpen_triggered()
{
    ui->statusBar->showMessage("Opened file...");
    qDebug() << "Opened";
}

void MainWindow::on_actionNew_triggered()
{
    (new MainWindow())->show();
}

void MainWindow::closeEvent(QCloseEvent *ev)
{
    if (isWindowModified())
    {
        int answer = QMessageBox::warning(this, "Unsaved changes",
                                          "You have unsaved changes!",
                                          QMessageBox::Discard | QMessageBox::Cancel);
        if (answer == QMessageBox::Discard)
        {
            ev->accept();
        }
        if (answer == QMessageBox::Cancel)
        {
            ev->ignore();
        }
    }
}
