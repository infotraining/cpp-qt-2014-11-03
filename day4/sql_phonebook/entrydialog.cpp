#include "entrydialog.h"
#include "ui_entrydialog.h"

EntryDialog::EntryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntryDialog)
{
    ui->setupUi(this);
}

EntryDialog::~EntryDialog()
{
    delete ui;
}

QString EntryDialog::Name()
{
    return ui->editName->text();
}

void EntryDialog::setName(QString name)
{
    ui->editName->setText(name);
}

QString EntryDialog::Phone()
{
    return ui->editPhone->text();
}

void EntryDialog::setPhone(QString phone)
{
    ui->editPhone->setText(phone);
}
