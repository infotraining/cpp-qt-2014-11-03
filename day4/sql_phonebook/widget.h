#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSqlTableModel>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlField>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

    QSqlTableModel* model;
    QSqlDatabase db;

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_btnAdd_clicked();

    void on_btnRemove_clicked();

    void on_editFilter_returnPressed();

    void on_btnClear_clicked();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
