#-------------------------------------------------
#
# Project created by QtCreator 2014-11-06T14:30:11
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sql_phonebook
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    entrydialog.cpp

HEADERS  += widget.h \
    entrydialog.h

FORMS    += widget.ui \
    entrydialog.ui
