#include "widget.h"
#include "ui_widget.h"
#include "entrydialog.h"
#include <QDebug>
#include <QSqlError>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../../phonebook.sl3");
    db.open();

    model = new QSqlTableModel(this, db);

    QStringList headers;

    ui->tableView->horizontalHeader()->setStretchLastSection(true);

    model->setTable("Phonebook");
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->select();
    ui->tableView->setModel(model);
    ui->tableView->hideColumn(0);
    qDebug() << db.lastError();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnAdd_clicked()
{
    EntryDialog dlg;
    if (dlg.exec() == QDialog::Accepted)
    {
        QSqlRecord record;
        record.append(QSqlField("NAME", QVariant::String));
        record.append(QSqlField("PHONE", QVariant::String));
        record.setValue("NAME", dlg.Name());
        record.setValue("PHONE", dlg.Phone());
        model->insertRecord(-1, record);
    }
}

void Widget::on_btnRemove_clicked()
{
    int row = ui->tableView->currentIndex().row();
    model->removeRow(row);
    model->select();
}


void Widget::on_editFilter_returnPressed()
{
    model->setFilter(QString("NAME LIKE \"\%%1\%\"").arg(ui->editFilter->text()));
    model->select();
    //qDebug() << db.lastError();
}

void Widget::on_btnClear_clicked()
{
    model->setFilter(QString(""));
    model->select();
}
