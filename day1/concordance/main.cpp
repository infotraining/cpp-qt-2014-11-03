#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QHash>
#include <QTime>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QFile file ("../alice.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Error opening file";
        return 1;
    }

    QTextStream stream(&file);
    QString text = stream.readAll();
    QStringList words;
//    QString word;
//    while( !stream.atEnd() )
//    {
//        stream >> word;
//        words << word;
//    }

    words = text.split(QRegExp("\\W+"), QString::SkipEmptyParts);
    //qDebug() << words;

    QHash<QString, int> freq;

    QTime timer;
    timer.start();
    foreach(QString word, words)
    {
        //qDebug() << word;
//        if (freq.contains(word))
//        {
//            freq[word]++;
//        }
//        else
//        {
//            freq.insert(word,1);
//            //++freq[word];
//        }
        ++freq[word.toLower()];
    }

    //qDebug() << freq;

    QMap<int, QStringList> result;

    QHashIterator<QString, int> it(freq);
    while(it.hasNext())
    {
        it.next();
        //qDebug() << it.key() << ": " << it.value();
        //result[it.value()].append(it.key());
        result[it.value()] << it.key();
    }

    qDebug() << timer.elapsed() << " ms";

//    qDebug() << result;

    QMap<int, QStringList>::iterator rit;
    int i = 0;
    rit = result.end();
    while(rit != result.begin() && i <= 30)
    {
        --rit;
        qDebug() << rit.value() << " : " << rit.key();
        ++i;
    }

    return 0;
    return a.exec();
}
