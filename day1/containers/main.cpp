#include <QCoreApplication>
#include <QDebug>
#include <QList>
#include <QString>
#include <QMap>
#include <QStringList>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QList<int> lista;
    lista << 1 << 2 << 3;
    qDebug() << lista;

    // STL-style iterators
    QList<int>::iterator it;
    for (it = lista.begin() ; it != lista.end() ; ++it)
        qDebug() << *it;

    // Java-style iterators
    QListIterator<int> i(lista);
    while(i.hasNext())
    {
        qDebug() << i.next();
    }

    // Qt-style
    foreach (int i, lista) {
       qDebug() << i;
    }

    // C++11
    for(auto& i : lista)
    {
        qDebug() << i;
    }

    // Hash containers
    QMap<QString, int> mapa;

    mapa["Ola"] = 100;
    mapa["Leszek"] = 200;
    mapa["Krystian"] = 300;

    qDebug() << mapa;

    QMapIterator<QString, int> itm(mapa);
    while(itm.hasNext())
    {
        itm.next();
        qDebug() << itm.value() << " : " << itm.key();
    }

    if (mapa.contains("Leszek"))
    {
        qDebug() << "Leszek has " << mapa["Leszek"];
    }

    qDebug() << mapa;

    for(auto elem : mapa)
    {
        qDebug() << elem;
    }

    QStringList strlist;
    strlist << "Ala" << "Ola" << "Ela";
    qDebug() << strlist;

    return 0;
    return a.exec();
}
