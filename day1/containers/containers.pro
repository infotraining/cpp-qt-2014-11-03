#-------------------------------------------------
#
# Project created by QtCreator 2014-11-03T12:13:57
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = containers
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

QMAKE_CXXFLAGS += -std=c++11
