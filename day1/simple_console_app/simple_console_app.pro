#-------------------------------------------------
#
# Project created by QtCreator 2014-11-03T10:34:47
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = simple_console_app
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

QMAKE_CXXFLAGS += -std=c++11
