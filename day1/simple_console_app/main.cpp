#include <QCoreApplication>
#include <QDebug>
#include <QString>

class MyClass : QObject
{
    QString id;
public:
    MyClass(QString text, QObject* parent = 0) :
        id(text), QObject(parent)
    {
        qDebug() << "Constructor";
    }
    ~MyClass()
    {
        qDebug() << "Destructor";
    }

};

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    qDebug() << "Hello World";

    QObject parent;
    MyClass *a, *b;
    a = new MyClass("a", &parent);
    b = new MyClass("b", &parent);
    return 0;
    return app.exec();
}
