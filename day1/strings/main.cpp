#include <QCoreApplication>
#include <QString>
#include <QDebug>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString str("Ala ma kota łoś");
    QString korean("예제 중심의 애플리케이션 개   발을 위한 Qt5 프로그래밍 가이드");
    qDebug() << str;
    std::cout << str.toStdString() << std::endl;
    QString num;
    num = QString::number(3.14159);
    qDebug() << num;

    QString filename("testowy.txt");
    if (filename.endsWith(".txt"))
    {
        qDebug() << "txt file = " << filename;
    }

    for (auto ch : korean)
    {
        qDebug() << ch;
    }
    return a.exec();
}
