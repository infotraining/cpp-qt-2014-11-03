#-------------------------------------------------
#
# Project created by QtCreator 2014-11-03T11:46:50
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = strings
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

QMAKE_CXXFLAGS += -std=c++11
