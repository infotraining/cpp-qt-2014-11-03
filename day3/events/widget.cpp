#include "widget.h"
#include "ui_widget.h"
#include <QPainter>
#include <QMessageBox>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    event->accept();
    QPoint coord = event->pos();
    //qDebug() << "pos: " << coord;
    QString text = QString("pos: %1, %2").arg(coord.x()).arg(coord.y());

    switch (event->modifiers())
    {
    case Qt::ShiftModifier:
        text += "\nShift";
        break;
    case Qt::ControlModifier:
        text += "\nCtrl";
        break;
    case Qt::AltModifier:
        text += "\nAlt";
        break;
    }

    ui->label->setText(text);
}

void Widget::mouseDoubleClickEvent(QMouseEvent *event)
{
    event->accept();
    QPoint coord = event->pos();
    //qDebug() << "pos: " << coord;
    QString text = QString("double clicked on: %1, %2").arg(coord.x()).arg(coord.y());
    ui->label->setText(text);
}

void Widget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setBrush(QBrush(QColor(100, 255, 255)));
    painter.drawRect(QRect(10,10, 50, 50));
}

void Widget::closeEvent(QCloseEvent *event)
{
    int ans = QMessageBox::warning(this, "Warning", "Are you sure you want to close?",
                                   QMessageBox::Ok | QMessageBox::Cancel);
    if (ans == QMessageBox::Cancel)
        event->ignore();
    else
        event->accept();
}
