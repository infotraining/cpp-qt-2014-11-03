#include "widget.h"
#include <QListWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    auto list = new QListWidget();
    auto btn_add = new QPushButton("Add");
    auto btn_remove =  new QPushButton("Remove");
    auto btn_edit =  new QPushButton("Edit");
    auto btn_close =  new QPushButton("Close");

    auto btn_l = new QHBoxLayout();
    btn_l->addWidget(btn_add);
    btn_l->addWidget(btn_remove);
    btn_l->addWidget(btn_edit);
    btn_l->addWidget(btn_close);

    auto l = new QVBoxLayout();
    l->addWidget(list);
    l->addLayout(btn_l);

    this->setLayout(l);

    connect(btn_close, &QPushButton::clicked, [this] () { this->close();});
}

Widget::~Widget()
{

}
