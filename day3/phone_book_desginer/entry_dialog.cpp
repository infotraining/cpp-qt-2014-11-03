#include "entry_dialog.h"
#include "ui_entry_dialog.h"

entry_dialog::entry_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::entry_dialog)
{
    ui->setupUi(this);
}

entry_dialog::~entry_dialog()
{
    delete ui;
}

QString entry_dialog::Name()
{
    return ui->edt_name->text();
}

void entry_dialog::setName(QString name)
{
    ui->edt_name->setText(name);
}

QString entry_dialog::Phone()
{
    return ui->edt_phone->text();
}

void entry_dialog::setPhone(QString phone)
{
    ui->edt_phone->setText(phone);
}
