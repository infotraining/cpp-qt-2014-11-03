#ifndef VISUALLABEL_H
#define VISUALLABEL_H

#include <QWidget>

namespace Ui {
class VisualLabel;
}

class VisualLabel : public QWidget
{
    Q_OBJECT

public:
    explicit VisualLabel(QWidget *parent = 0);
    ~VisualLabel();

    QString Name();
    void setName(QString name);
    QString Phone();
    void setPhone(QString phone);

private:
    Ui::VisualLabel *ui;
};

#endif // VISUALLABEL_H
