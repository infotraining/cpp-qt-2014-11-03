#include "widget.h"
#include "ui_widget.h"
#include "entry_dialog.h"
#include "visuallabel.h"
#include <QDebug>
#include <QLabel>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btn_add_clicked()
{
    entry_dialog dlg;
    if (dlg.exec() == QDialog::Accepted)
    {
        //ui->listWidget->addItem(QString("%1 :: %2").arg(dlg.Name(), dlg.Phone()));
        QString msg = QString("%1 :: %2").arg(dlg.Name(), dlg.Phone());
        QListWidgetItem* item = new QListWidgetItem();
        VisualLabel* label = new VisualLabel();
        label->setName(dlg.Name());
        label->setPhone(dlg.Phone());
        item->setSizeHint(QSize(0, 100));
        ui->listWidget->addItem(item);
        ui->listWidget->setItemWidget(item, label);
    }
}

void Widget::on_btn_remove_clicked()
{
    //QListWidgetItem* item =  ui->listWidget->currentItem();
    foreach(QListWidgetItem* item, ui->listWidget->selectedItems())
    {
        delete item;
    }
}

void Widget::on_btn_edit_clicked()
{
    entry_dialog dlg;
    QListWidgetItem* item = ui->listWidget->currentItem();
    if (item)
    {
        //QStringList data = item->text().split(" :: ");
        VisualLabel* label = dynamic_cast<VisualLabel*>(ui->listWidget->itemWidget(item));
        dlg.setName(label->Name());
        dlg.setPhone(label->Phone());

        if (dlg.exec() == QDialog::Accepted)
        {
            label->setName(dlg.Name());
            label->setPhone(dlg.Phone());
        }
    }
}
