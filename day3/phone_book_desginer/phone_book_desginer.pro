#-------------------------------------------------
#
# Project created by QtCreator 2014-11-05T09:59:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = phone_book_desginer
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    entry_dialog.cpp \
    visuallabel.cpp

HEADERS  += widget.h \
    entry_dialog.h \
    visuallabel.h

FORMS    += widget.ui \
    entry_dialog.ui \
    visuallabel.ui
