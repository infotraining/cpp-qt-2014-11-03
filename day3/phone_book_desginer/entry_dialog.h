#ifndef ENTRY_DIALOG_H
#define ENTRY_DIALOG_H

#include <QDialog>

namespace Ui {
class entry_dialog;
}

class entry_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit entry_dialog(QWidget *parent = 0);
    ~entry_dialog();

    // getters and setters
    QString Name();
    void setName(QString name);
    QString Phone();
    void setPhone(QString phone);

private:
    Ui::entry_dialog *ui;
};

#endif // ENTRY_DIALOG_H
