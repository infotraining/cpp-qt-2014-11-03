#include "visuallabel.h"
#include "ui_visuallabel.h"

VisualLabel::VisualLabel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VisualLabel)
{
    ui->setupUi(this);
}

VisualLabel::~VisualLabel()
{
    delete ui;
}

QString VisualLabel::Name()
{
    return ui->labelName->text();
}

void VisualLabel::setName(QString name)
{
    ui->labelName->setText(name);
}

QString VisualLabel::Phone()
{
    return ui->labelPhone->text();
}

void VisualLabel::setPhone(QString phone)
{
    ui->labelPhone->setText(phone);
}
