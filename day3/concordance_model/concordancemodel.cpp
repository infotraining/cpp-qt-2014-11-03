#include "concordancemodel.h"
#include <QDebug>

void ConcordanceModel::update_model()
{
    freq_list.clear();
    QStringList words;
    QRegExp spl(split_);
    if (!spl.isValid())
        spl = QRegExp("\\W+");

    words = text_.split(spl);

    if (words.size() == 0)
    {
        return;
    }


    QHash<QString, int> freq;
    foreach (const QString& word, words)
       freq[word.toLower()]++;

    QMap<int, QStringList> result;
    QHashIterator<QString, int> it(freq);
    while (it.hasNext())
    {
        it.next();
        result[it.value()] << it.key();
    }

    int rows = 0;

    beginResetModel();
    if (result.size() == 0)
    {
        freq_list.clear();
        endResetModel();
        return;
    }

    QMap<int, QStringList>::iterator itt;

    for (itt = result.end()-1 ; itt != result.begin() ; --itt)
    {
        QString words_;
        foreach (QString word, itt.value())
        {
            words_ += word;
            words_ += ", ";
        }
        freq_list.append(QPair<int, QString>(itt.key(), words_));
    }
    endResetModel();
}

ConcordanceModel::ConcordanceModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    split_ = QString("\\W+");
    text_ = QString("");
}

int ConcordanceModel::rowCount(const QModelIndex &parent) const
{
    return freq_list.size();
}

int ConcordanceModel::columnCount(const QModelIndex &parent) const
{
    return 2;
}

QVariant ConcordanceModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (index.column() == 0)
            return freq_list[index.row()].first;
        else
            return freq_list[index.row()].second;
    }
    else
    {
        return QVariant::Invalid;
    }
}

QVariant ConcordanceModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            if(section == 0)
                return "Count";
            if(section == 1)
                return "Word(s)";
        }

    }
    return QVariant::Invalid;
}

void ConcordanceModel::setText(QString text)
{
    text_ = text;
    update_model();
}

void ConcordanceModel::setSplit(QString text)
{
    split_ = text;
    update_model();
}
