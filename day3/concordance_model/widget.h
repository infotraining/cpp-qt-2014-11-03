#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "concordancemodel.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_btnOpen_clicked();

    void on_edtSplit_returnPressed();

    void on_plainTextEdit_textChanged();

private:
    Ui::Widget *ui;
    ConcordanceModel *model;
};

#endif // WIDGET_H
