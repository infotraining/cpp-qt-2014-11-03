#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include "concordancemodel.h"
#include <QTextStream>
#include <QFile>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    model = new ConcordanceModel();
    ui->tableView->setModel(model);
    model->setText(ui->plainTextEdit->toPlainText());
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnOpen_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open file for concordance",
                                                    ".",
                                                    "All files (*.*);;Text(*.txt)");
    if (!filename.isEmpty())
    {
        QFile f(filename);
        if (f.open(QFile::ReadOnly | QFile::Text))
        {
            QTextStream in(&f);
            QString all_text = f.readAll();
            model->setText( all_text );
            ui->plainTextEdit->setPlainText( all_text );
        }
    }
}

void Widget::on_edtSplit_returnPressed()
{
    model->setSplit(ui->edtSplit->text());
}

void Widget::on_plainTextEdit_textChanged()
{
    model->setText(ui->plainTextEdit->toPlainText());
}
