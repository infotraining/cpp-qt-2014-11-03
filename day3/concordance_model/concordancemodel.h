#ifndef CONCORDANCEMODEL_H
#define CONCORDANCEMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include <QPair>
#include <QStringList>
#include <QMap>
#include <QHash>

class ConcordanceModel : public QAbstractTableModel
{
    Q_OBJECT

    QVector< QPair<int, QString> > freq_list;
    QString text_, split_;
    void update_model();

public:
    explicit ConcordanceModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

signals:

public slots:
    void setText(QString text);
    void setSplit(QString text);

};

#endif // CONCORDANCEMODEL_H
