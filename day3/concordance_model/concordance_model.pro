#-------------------------------------------------
#
# Project created by QtCreator 2014-11-05T15:01:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = concordance_model
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    concordancemodel.cpp

HEADERS  += widget.h \
    concordancemodel.h

FORMS    += widget.ui
