#-------------------------------------------------
#
# Project created by QtCreator 2014-11-05T14:09:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = simple_model
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    test_model.cpp

HEADERS  += widget.h \
    test_model.h

FORMS    += widget.ui
