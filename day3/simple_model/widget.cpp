#include "widget.h"
#include "ui_widget.h"
#include <QStandardItemModel>
#include "test_model.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
//    QStandardItemModel* model = new QStandardItemModel(5, 2);

//    for (int row = 0 ; row < 5 ; ++row)
//    {
//        for (int col = 0 ; col < 2 ; ++col)
//        {
//            QStandardItem *item =
//                    new QStandardItem(QString("row %1, col %2").arg(row).arg(col));
//            if (col == 0)
//            {
//                QStandardItem* child = new QStandardItem("Child");
//                item->appendRow(child);
//            }
//            model->setItem(row, col, item);
//        }
//    }

    test_model* model = new test_model();

    ui->tableView->setModel(model);
    ui->treeView->setModel(model);
    ui->listView->setModel(model);
    //ui->columnView->setModel(model);
}

Widget::~Widget()
{
    delete ui;
}
