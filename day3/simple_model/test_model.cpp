#include "test_model.h"

test_model::test_model(QObject *parent) :
    QAbstractTableModel(parent)
{

}

int test_model::rowCount(const QModelIndex &parent) const
{
    return 5;
}

int test_model::columnCount(const QModelIndex &parent) const
{
    return 2;
}

QVariant test_model::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        return QString("%1 , %2").arg(index.column()).arg(index.row());
    }
    else
    {
        return QVariant::Invalid;
    }
}

QVariant test_model::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            if(section == 0)
                return "First";
            if(section == 1)
                return "Second";
        }

    }
    return QVariant::Invalid;
}

