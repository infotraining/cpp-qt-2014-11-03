#ifndef TEST_MODEL_H
#define TEST_MODEL_H

#include <QAbstractTableModel>

class test_model : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit test_model(QObject *parent = 0);

    // indispensable functions
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

signals:

public slots:

};

#endif // TEST_MODEL_H
