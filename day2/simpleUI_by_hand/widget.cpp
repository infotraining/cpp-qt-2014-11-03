#include "widget.h"
#include <QDial>
#include <QSlider>
#include <QSpinBox>
#include <QVBoxLayout>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QDial* dial = new QDial();
    QSlider* slider = new QSlider();
    slider->setOrientation(Qt::Horizontal);
    QSpinBox* spinbox = new QSpinBox();

    QVBoxLayout* l = new QVBoxLayout();
    l->addWidget(dial);
    l->addWidget(slider);
    l->addWidget(spinbox);

    this->setLayout(l);

    connect(dial, &QDial::valueChanged, slider, &QSlider::setValue);
    connect(slider, &QSlider::valueChanged, spinbox, &QSpinBox::setValue);
    connect(spinbox, SIGNAL(valueChanged(int)), dial, SLOT(setValue(int)));
    connect(spinbox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
}

Widget::~Widget()
{

}
