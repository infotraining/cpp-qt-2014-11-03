#-------------------------------------------------
#
# Project created by QtCreator 2014-11-04T16:04:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = simpleUI_by_hand
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
