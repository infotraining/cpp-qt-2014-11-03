#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->table->horizontalHeader()->setStretchLastSection(true);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnOpen_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open file for concordance",
                                                    ".",
                                                    "All files (*.*);;Text(*.txt)");
    qDebug() << "Filename " << filename;
    if (!filename.isEmpty())
    {
        QFile f(filename);
        if (f.open(QFile::ReadOnly | QFile::Text))
        {
            QTextStream in(&f);
            ui->textEdit->setPlainText( f.readAll() );
        }
    }
    calculate();
}

void Widget::calculate()
{
    qDebug() << "calculating...";

    QString re = ui->edtFilter->text();
    QStringList words = ui->textEdit->toPlainText().split(QRegExp(re), QString::SkipEmptyParts);
    if (words.size() == 0)
    {
        return;
    }

    QHash<QString, int> freq;

    foreach(QString word, words)
    {
        ++freq[word.toLower()];
    }

    QMap<int, QStringList> result;

    QHashIterator<QString, int> it(freq);
    while (it.hasNext()) {
        it.next();
        result[it.value()] << it.key();
    }

    qDebug() << result;

    int rows = 0;
    ui->table->clear();
    ui->table->setRowCount(0);
    QMap<int, QStringList>::iterator itt;
    for (itt = result.end()-1 ; itt != result.begin() ; --itt, rows++)
    {
        ui->table->insertRow(rows);
        QString val = itt.value().join(" ");
        QString fr = QString::number(itt.key());
        ui->table->setItem(rows, 0, new QTableWidgetItem(fr));
        ui->table->setItem(rows, 1, new QTableWidgetItem(val));
    }
}

