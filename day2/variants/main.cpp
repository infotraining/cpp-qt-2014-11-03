#include <QCoreApplication>
#include <QVariant>
#include <QDebug>
#include <QQueue>

class MyType
{
    QString m_content;
public:
    MyType() : m_content() {}
    MyType(QString content) : m_content(content) {}
    ~MyType() {}

    QString content() const
    {
        return m_content;
    }
};

Q_DECLARE_METATYPE(MyType)

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QVariant v1(123);
    int x = v1.toInt();
    qDebug() << x;
    qDebug() << "To String " << v1.toString();
    qDebug() << "type      " << v1.type();
    QVariant v2("Leszek");
    qDebug() << v2 << " " << v2.toString();
    qDebug() << "Can convert? " << v2.canConvert<int>();
    qDebug() << v2.toInt();

    qRegisterMetaType<MyType>();
    MyType data(QString("Hello World"));

    QVariant v3;
    v3.setValue(data);

    qDebug() << "QVariant " << v3;
    qDebug() << "type     " << v3.type();
    qDebug() << "typeName " << v3.typeName();
    qDebug() << "userType " << v3.userType();
    qDebug() << "can convert to int " << v3.canConvert<int>();
    qDebug() << "can convert to MyType " << v3.canConvert<MyType>();
    qDebug() << v3.value<MyType>().content();
    return 0;
    return a.exec();
}
