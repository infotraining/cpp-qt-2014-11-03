#include "myclass.h"

MyClass::MyClass(QObject *parent) :
    QObject(parent), value_(0)
{
}

int MyClass::value()
{
    return value_;
}

void MyClass::setValue(int value)
{
    if (value != value_)
    {
        value_ = value;
        emit valueChanged(value_);
    }
}
