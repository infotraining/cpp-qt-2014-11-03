#-------------------------------------------------
#
# Project created by QtCreator 2014-11-04T11:47:10
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = signals_and_slots
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    myclass.cpp

HEADERS += \
    myclass.h

CONFIG += c++11
