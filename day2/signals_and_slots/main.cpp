#include <QCoreApplication>
#include <QDebug>
#include "myclass.h"

void print(int value)
{
    qDebug() << "Value is set to " << value;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MyClass m1, m2;

    QObject::connect(&m2, &MyClass::valueChanged, &print); // since Qt 5.x
    QObject::connect(&m1, &MyClass::valueChanged,
                     [] (int value) {
                                        qDebug() << "Lambda observer got " << value;
                                    } );

    m1.setValue(100);
    m2.setValue(200);
    qDebug() << "m1 value = " << m1.value();
    qDebug() << "m2 value = " << m2.value();

    QObject::connect(&m1, SIGNAL(valueChanged(int)),
                     &m2, SLOT(setValue(int)));

    m1.setValue(1000);
    qDebug() << "m1 value = " << m1.value();
    qDebug() << "m2 value = " << m2.value();

    m2.setValue(2000);
    qDebug() << "m1 value = " << m1.value();
    qDebug() << "m2 value = " << m2.value();

    QObject::connect(&m2, &MyClass::valueChanged,
                     &m1, &MyClass::setValue);

    m2.setValue(3000);
    qDebug() << "m1 value = " << m1.value();
    qDebug() << "m2 value = " << m2.value();

    return 0;
    return a.exec();
}
