#ifndef MYCLASS_H
#define MYCLASS_H

#include <QObject>

class MyClass : public QObject
{
    Q_OBJECT
public:
    explicit MyClass(QObject *parent = 0);
    int value();

signals:
    void valueChanged(int value);

public slots:
    void setValue(int value);

private:
    int value_;
};

#endif // MYCLASS_H
